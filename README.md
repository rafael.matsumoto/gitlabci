# Repositório de teste de integração e deploy usando pipelines do Gitlab

Para rodar o site:

```sh
$ yarn
```

```sh
$ yarn start
```

Live demo: http://erratic-pie.surge.sh/

Exemplo de configuração do CI no arquivo [.gitlab-ci.yml](https://gitlab.com/rafael.matsumoto/gitlabci/blob/master/.gitlab-ci.yml)