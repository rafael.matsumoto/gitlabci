---
id: doc1
title: Deploy suceeded
sidebar_label: Example Page
---

Check the [documentation](https://docusaurus.io) for how to use Docusaurus.

# Título

Essa página de documentação é um exemplo de uma main page da documentação

## Como funciona um exemplo de código

```js
const sum = (a, b) {
    return a + b;
}
```